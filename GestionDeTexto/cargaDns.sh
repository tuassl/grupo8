#!/bin/bash

if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
	mkdir log &> /dev/null
	chmod 777 log &> /dev/null
fi

awk '
BEGIN {
  i=0;
  n=0;
  print "# Modificado por Grupo8" > "./log/resolv.conf";
}
NR==FNR {
  i++;
  dns[i]=$1;
  next;
}
{
  if ($0 !~ /^[ \t]*(#|;|$)/) {
    if (i>n) {
      n++;
      if (n<=3) {
        print "nameserver ", dns[n], " # Agregado ", $0 >> "./log/resolv.conf";
      }
      else {
        print "# nameserver ", dns[n]," Agregado y Comentado ", $0 >> "./log/resolv.conf";
      }
    }
    else {
      print "# Comentado ", $0 >> "./log/resolv.conf";
    }
  }
  else {
    print $0 >> "./log/resolv.conf";
  }
}
END {
  while (n<i) {
    n++;
    if (n<=3) {
      print "nameserver ", dns[n], " # Agregado" >> "./log/resolv.conf";
    }
    else {
      print "# nameserver ", dns[n], " # Agregado y Comentado" >> "./log/resolv.conf";
    }
  }
}
' $1 /etc/resolv.conf
#cambiar por verificaRoot
if [ `id -u` -ne 0 ] ; then
	#cambiar por cartel
	cartel "Debe copiar el archivo \"resolv.conf\" que se encuentra en el directorio \"log\" hacia el directorio \"/etc\""
	chmod 777 log/resolv.conf &> /dev/null
	cat ./log/resolv.conf
	borde
else
	mv -f /etc/resolv.conf /etc/resolv.conf.bkp
	mv -f ./log/resolv.conf /etc/resolv.conf
	cartel "Se cargadon los DNS que se encontraban en el archivo $1"
	cat /etc/resolv.conf
	borde
fi
