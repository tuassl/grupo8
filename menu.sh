#!/bin/bash
#programa principal
#accede a los menus a traves de funciones en el archivo funcionmenu
#utiliza funciones adicionales como verifiar root

source funcionesGenerales.sh;

menuPrincipal(){

	clear
	cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"

	while true ; do

		cartel "Menu principal"
		centro "1)Gestion de usuarios"
		centro "2)Gestion de discos"
		centro "3)Gestion de procesos"
		centro "4)Gestion de la memoria"
		centro "5)Gestion de texto"
		borde
		centro "9)Ayuda General"
		centro "0)Salir"
		borde
                cartel "NOTA: Por cada reporte que se realice se genera una copia que podra verse dentro la carpeta log alojada en la carpeta grupo8."
		read -p "Ingrese una opcion: " OPCION
		case $OPCION in

			1)
				clear
				cartel "Ingresando al Menu de Gestion de Usuarios"
				menuGestionUsuarios
				;;
			2)
				clear
				cartel "Reporte de Gestion de discos"
				menuGestionDiscos
				;;
			3)
				clear
				cartel "Reporte de procesos"
				menuGestionProcesos
				;;
			4)
				clear
				cartel "Reporte de Memoria"
				menuGestionMemoria
				;;
			5)
				clear
				cartel "Reporte de Gestion de Texto"
				menuGestionTexto
				;;
			9)
				clear
				cartel "Ayuda General"
				ayuda general
				;;
			0)
				clear
				cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
				break
				;;
			*)
				clear
				cartel "Opcion Incorrecta, Intente de nuevo"
				;;
		esac
	done
}

menuGestionUsuarios(){

	clear
	cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
	while true ; do

		cartel "Menu Gestion de Usuarios"
		centro "1)Alta de usuarios"
		centro "2)Cruce de claves"
		centro "3)Baja de usuarios"
		centro "4)Reporte de usuarios del sistema"
		centro "5)Reporte de usuarios activos"
		borde
		centro "9)Ayuda Gestion de Usuarios"
		centro "0)Atras"
		borde

		read -p "Ingrese una opcion: " OPCION

		case $OPCION in

			1)
				clear
				cartel "Proceso de Alta de usuario"
				cartel "Se crear los usuarios que se encuentran en el archivo grupo.csv"
				bash GestionUsuarios/altaDeUser.sh -g grupo.csv
				;;
			2)
				clear
				cartel "Proceso de Cruce de claves"
				bash GestionUsuarios/cruzauser.sh -g grupo.csv -r red.csv
				;;
			3)
				clear
				cartel "Proceso de Baja de usuarios"
				cartel "Se eliminaran los usuarios que se encuentran en el archivo grupo.csv creando el backup respectivo"

				bash GestionUsuarios/bajaDeUser.sh -b -g grupo.csv
				;;
			4)
				clear
				cartel "Proceso de Reporte de usuarios del sistema"
				cartel "Creara un reporte de los usuarios exclusivos de sistema"
				bash GestionUsuarios/listadoUsuarios.sh -s -m
				;;
			5)
				clear
				cartel "Proceso de Reporte de usuarios activos"
				cartel "Creara un reporte de los usuarios que tengan una shell valida"
				bash GestionUsuarios/listadoUsuarios.sh -l -m
				;;
			9)
				cartel "Ayuda Gestion Usuario"
				ayuda usuarios
			;;
			0)
				clear
				cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
				return
				;;
			*)
				clear
				cartel "OPCION NO VALIDA, Intente de Nuevo"
				;;
		esac
	done
}

menuGestionDiscos(){

	clear
	cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
	while true ; do

		cartel "Menu Gestion de Discos"
		centro "1)Reporte de información"
		centro "2)Reporte de rendimiento"
		centro "3)Reporte de actividad"
		borde
		centro "9)Ayuda Gestion de Discos"
		centro "0)Atras"
		borde

		read -p "Ingrese una opcion: " OPCION

		case $OPCION in

			1)
				clear
				cartel "Reporte de Informacion"
				bash GestionDeDisco/informacionDisco.sh -r red.csv
				;;
			2)
				clear
				cartel "Reporte de Rendimineto"
				bash GestionDeDisco/rendimientoDeDisco.sh
				;;
			3)
				clear
				cartel "Reporte de Actividad"
				bash GestionDeDisco/reporteActIo.sh
				;;
			9)
				clear
				cartel "Ayuda Gestion de Discos"
				ayuda discos
				;;
			0)
				clear
				cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
				return
				;;
			*)
				clear
				cartel "OPCION NO VALIDA, Intente de Nuevo"
				;;
		esac
	done
}


menuGestionProcesos(){

	clear
	cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
	while true ; do

		cartel "Menu Gestion de Procesos"
		centro "1)Reporte 5 Procesos Ordenados por Consumo de Memoria"
		centro "2)Reporte de rendimiento"
		centro "3)Monitorear un proceso específico"
		centro "4)Eliminar un proceso específico dado su id"
		centro "5)Eliminar una lista de procesos "
		borde
		centro "9)Ayuda Gestion de Procesos "
		centro "0)Atras"
		borde

		read -p "Ingrese una opcion: " OPCION

		case $OPCION in

			1)
				clear
				cartel "Reporte 5 Procesos Ordenados por Consumo de Memoria"
				bash GestionDeProcesos/reporteprimerosproc.sh
				;;
			2)
				clear
				cartel "Reporte de Rendimineto"
				bash GestionDeProcesos/reporteDeProcesos.sh
				;;
			3)
				clear
				cartel "Monitorear un proceso específico"
				bash GestionDeProcesos/monitorepid.sh
				;;
			4)
				clear
				cartel "Eliminar un proceso específico dado su id"
				bash GestionDeProcesos/eliminarProcesos.sh
				;;
			5)
				clear
				cartel "Eliminar una lista de procesos"
				bash GestionDeProcesos/eliminaPidsPorLista.sh
				;;
			9)
				clear
				cartel "Ayuda Gestion de procesos"
				ayuda procesos
				;;
			0)
				clear
				cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
				return
				;;
			*)
				clear
				cartel "OPCION NO VALIDA, Intente de Nuevo"
				;;
		esac
	done
}


menuGestionMemoria(){

	clear
	cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
	while true ; do

		cartel "Menu Gestion de Memoria"
		centro "1)Reporte del Uso de Memoria RAM"
		centro "2)Memory Alert"
		borde
		centro "9)Ayuda Gestion de Memoria"
		centro "0)Atras"
		borde

		read -p "Ingrese una opcion: " OPCION

		case $OPCION in

			1)
				clear
				cartel "Reporte del Uso de Memoria RAM"
				bash GestionDeMemoria/reporteusomemoria.sh
				;;
			2)
				clear
				cartel "Memory Alert"
				bash GestionDeMemoria/advertenciaMailboxLocal.sh
				;;
			9)
				clear
				cartel "Ayuda Gestion de Memoria"
				ayuda memoria
				;;
			0)
				clear
				cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
				return
				;;
			*)
				clear
				cartel "OPCION NO VALIDA, Intente de Nuevo"
				;;
		esac
	done
}

menuGestionTexto(){

	clear
	cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
	while true ; do

		cartel "Menu Gestion de Texto"
		centro "1)Verificar Hostname"
		centro "2)Listado de Servidores DNS"
		borde
		centro "9)Ayuda Gestion de Texto"
		centro "0)Atras"
		borde

		read -p "Ingrese una opcion: " OPCION

		case $OPCION in

			1)
				clear
				cartel "Verificar Hostname"
				bash GestionDeTexto/verifHostnameLocal.sh
				;;
			2)
				clear
				cartel "Listado de Servidores DNS"
				bash GestionDeTexto/cargaDns.sh dns.csv
				;;
			9)
				clear
				cartel "Ayuda Gestion de Texto"
				ayuda texto
				;;
			0)
				clear
				cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
				return
				;;
			*)
				clear
				cartel "OPCION NO VALIDA, Intente de Nuevo"
				;;
		esac
	done
}


menuPrincipal
cartel $(ayuda version)

