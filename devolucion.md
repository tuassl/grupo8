# Grupo 8
---------
### Alumno:
- Martin Bonfanti
- Marcelo Bouzo
- Jose Centena
- Mauro Saracini

#### Branch evaluada: master

#Etapas del proyecto

###Menu: 100%
-----
OK! Muy completo!

###Usuarios: 65%
--------
- Funcionalidad extra!! que bien! encritado y verificación de existencia!
- ¿Que paso con las altas y bajas vía ssh?

###Discos: 85%
-------
- Faltó reporte de actividad I/O en forma remota, como lo hizo en el primer punto.

###PROCESOS: 90%
---------
- Si el pid no existe no debería mostrarlo, simplemente un mensaje que ese pid no existe.
- Si el pid no existe, dice que lo eliminó amablemente.

###MEMORIA: 100%
--------
OK!

###TEXTO: 100%
------
OK!

#Devolución general:
-------------------
El proyecto está muy bien!. El código está correcto y satisface el objetivo de este trabajo incluso tiene algunos agregados interesantes. Ojo que faltaron en algunos ejercicios poder realizar la misma tarea en forma remota, es raro porque en otros lo resolvieron muy bien.

Puntuación general del trabajo <b>9 pts.</b> Felicitaciones!

