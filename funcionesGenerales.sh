#!/bin/bash
# este script contiene las distintas funciones que se usan en los scripts
# las funciones son:
# verificaroot
# borde
# centro
# cartel
# randpass
# verificaComando

verificarRoot(){
	#Debe ser root para ejecutar el script
	if [ `id -u` -ne 0 ]; then
		cartel "Debe ser Administrador para ejecutar esta aplicacion. Adios!"
		exit 1
	fi
}
borde(){
	#hace una linea de guiones "-" de 80 caracteres para usar como borde
	for guion in {1..79} ; do
		echo -n "-"
	done
	echo
}
centro(){
	#corta el texto cada 75 caracteres para enmarcarlo con el borde
	#texto="${@^^}"	cambia a mayusculas
	#desactivado para poder integrar las llaves que son case sensitive
	texto="$@"
	while (( ${#texto} >= 74 )) ; do
		echo "| ${texto:0:74}  |"
		texto=${texto:74:((${#texto}-74))};
	done
	echo -ne "| $texto"
	for espacio in $(seq 1 $((74-${#texto}))) ; do
		echo -ne " "
	done
	echo "  |"
}

cartel(){
	#muestra un cartel con el texto
	borde
	centro $@
	borde
}

randPass(){
	#genera una pass aleatoria. si se para argumento , es el largo de la pass
	datos="abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ1234567890"
	pass=""
	pos=1
	[ -z "$1" ] && largo=6 || largo=$1
	while [ ${pos} -le $largo ]; do
		pass="$pass${datos:$(($RANDOM%${#datos})):1}"
		pos=$(($pos + 1))
	done
	echo $pass
}

#Se crea la funcion verifica comando

verificaComando() {

declare -u OPCION

    if [ -n "$(which $1)" ]; then
        echo "$(which $1)"
     else
        if [ -n "$(which dpkg)" ]; then
            dpkg -s $1 &> /dev/null
         if [ "$?" -eq "0" ]; then
             return
          else
             while true ; do
                echo "El comando $1 no se encontro"
                read -p "Se desea instalar? (Y/N): " OPCION
                case $OPCION in
                  Y|y)
                      echo "Vamos a instalar el comando $1"
                      apt-get update &>/dev/null
                      apt-get -y install $1 &>/dev/null
                      break
                  ;;
                  N|n)
                      exit
                  ;;
                  *)
                      echo "No existe la opcion"
                  ;;
                esac
             done
         fi
         else
            echo "El Comando no se cuentra instalado debera instalarlo de forma manual"
        fi
    fi
}

#Se crea funcion ayuda para usuarios

function ayuda() {

	case $1 in	#selecciona parameto de entrada para mostrar ayuda o errores
		general)
			echo "
GESTION DE USUARIOS:
	En esta opcion se accede al menu de usuarios, donde se podran crear o 
	eliminar usuarios a partir de un archivo. Generar y cruzar la llave de 
	varios usuarios para poder acceder a una lista de equipos. Generar un 
	listado tanto de usuarios de sistema, como usuarios activos con shell.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
GESTION DE DISCOS:
	En esta opcion se generan reportes de los discos ya sean locales como 
	remotos. Reportes de rendimiento y reporte actividad local.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
GESTION DE PROCESOS:
	En esta opcion se analizan los procesos. Se pueden ordenar los 
	procesos que consumen mas memoria, ya sean locales o remotos. 
	Monitorear un proceso especifico y eliminar un proceso o una lista.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
GESTION DE MEMORIA:
	En esta opcion se podra realizar un reporte de la memoria RAM y 
	ejecutar el script \"Memory Alert\" que nos envia un mail local si el 
	consumo de memoria supera cierto umbral.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
GESTION DE TEXTO:
	En esta opcion se podra verificar que exista nuestro hostname dentro 
	del archivo \"/etc/hosts\" y agregar DNS que sean necesarios.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			clear
			cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
		;;
		usuarios)
			echo "
ALTA DE USUARIOS (root):
	Con esta opcion se crean usuarios nuevos que estan detallados en el 
	archivo \"grupo.csv\" que consta de usuario y clave separados por 
	\",\". Ademas se genera, dentro de la carpeta \"log\", un archivo de 
	logueo con el nombre \"alta_usuarios.csv\" que registra todos los 
	usuarios procesados y un archivo para cada usuario donde se detalla 
	cada proceso que se realizo.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
CRUCE DE CLAVES (root):
	Con esta opcion se cruzan las llaves de los usuarios que estan 
	detallados en el archivo \"grupo.csv\" que consta de usuario y clave 
	separados por \",\" hacia los equipos que estan en el archivo 
	\"red.csv\" que consta de direcciones ip y puerto. Ademas se genera, 
	dentro de la carpeta \"log\", un archivo de logeo con el nombre 
	\"llave_usuario.csv\" que registra los usuarios y las llaves creadas 
	asi como tambien los accesos a los servidores y tambien un archivo 
	para cada usuario donde se detalla cada proceso que se realizo.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
BAJA DE USUARIOS (root):
	Con esta opcion se crean usuarios nuevos que estan detallados en el 
	archivo \"grupo.csv\" que consta de usuario y clave separados por 
	\",\". Ademas se genera, dentro de la carpeta \"log\", un archivo de 
	logueo con el nombre \"baja_usuarios.csv\" que registra todos los 
	usuarios procesados y un archivo para cada usuario donde se detalla 
	cada proceso que se realizo. Los datos de los usuarios procesados 
	quedan guardados en \"/home\" en un archivo comprimido tar.bz2.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
REPORTE DE USUARIOS DEL SISTEMA:
	Con esta opcion se crea un archivo con un listado de usuarios que 
	pertenecen al sistema. Este archivo se guarda dentro de la carpeta 
	\"log\", con el nombre \"lista_sistemas.csv\"
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
REPORTE DE USUARIOS ACTIVOS:
	Con esta opcion se crea un archivo con un listado de usuarios activos 
	que no son de sistema y que poseen una shel valida. Este archivo se 
	guarda dentro de la carpeta \"log\", con el nombre 
	\"lista_sistemas.csv\"
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			clear
			cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
		;;
		discos)
			echo "
REPORTE DE INFORMACION:
	Esta opcion lo que haces es conectase a maquina remota por ssh y 
	consulta la informacion del disco principal de la maquina
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
REPORTE DE RENDIMIENTO (root):
	Reporta por pantalla el rendimiento de los discos locales, en caso que 
	la herramienta no se encuentre instalada, el script deberá notificar 
	al usuario y ofrecerle la posibilidad de instalarla (Ayuda: sysstat).
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
REPORTE DE ACTIVIDAD (root):
	Reporte de actividad de I/O en todos los discos primarios de todos los 
	servidores provistos. Deberá obtener las lecto/escrituras del comando 
	que se está ejecutando en ese momento. Al igual que en el caso 
	anterior, en caso que la herramienta no se encuentre instalada, el 
	script deberá notificar al usuario y ofrecerle la posibilidad de 
	instalarla 
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			clear
			cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
		;;
		procesos)
			echo "
REPORTE POR CONSUMO DE MEMORIA:
	En esta opcion se reportan los 5 procesos ordenados por consumo de 
	memoria. Se puede realizar el reporte de la maquina local, de una IP 
	especifica o un archivo \"csv\" con el listado de equipos remotos.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
REPORTE DE RENDIMIENTO (root):
	Aqui lo que analiza es la cantidad de procesos en ejecucion y si 
	supera el umbral soliciado. En caso de ser superior al umbral, nos 
	informara mostrando un \"top\" con las primeras 10 lineas y luego un 
	\"ps\" con las primeras 10 lineas tambien.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
MONITOREO DE UN PROCESO:
	En esta opcion lo que realiza es un monitoreo de un proceso especifico 
	de acuerdo a su pid. Analiza el estado realizando una repeticion de 10 
	instancias con un tiempo de espera de 4 segundos entre una y otra.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
ELIMINAR PRODESO POR ID (root):
	En esta instancia se podra eliminar pasando un pid especifico. En caso 
	de eliminarlo o no, lo informara con un mensaje. Luego preguntara si 
	desea eliminar otro proceso.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
ELIMINAR LISTA DE PROCESOS (root):
	Aqui se generara un archivo \"csv\" que contiene una lista de pid que 
	luego seran eliminados.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			clear
			cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
		;;
		memoria)
			echo "
REPORTE DE USO DE MEMORIA:
	Con esta opcion se genera un reporte por pantalla del estado de la 
	memoria con los comandos \"free\", \"vmstat\" y \"meminfo\". Se puede 
	realizar el reporte de la maquina local, de una IP especifica o un 
	archivo \"csv\" con el listado de equipos remotos.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
MEMORY ALERT:
	Aqui se hace uso del script \"memory alert\" y dado un umbral 
	solicitado informa al usuario en caso de ser superior, mediante el 
	mailbox local y generando un reporte del consumo. El script \"memory 
	alert\" informa mediante los comandos \"top\" y \"ps\".
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			clear
			cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
		;;
		texto)
			echo "
VERIFICAR HOSTNAME:
	Aqui verifica que el hostname del equipo se encuentre correctamente 
	configurado dentro del archivo \"/etc/hosts\" informando por pantalla.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			echo "
CARGA LISTADO DE SERVIDORES DNS:
	En esta opcion se leera el contenido del archivo \"dns.csv\" y se 
	agregara al archivo \"/etc/resolv.conf\". En caso de no tener permisos 
	de superusuario, deja en la carpeta \"log\" el archivo \"resolv.conf\" 
	para colocarlo manualmente donde deberia.
"
			# read -n 1 -s -r -p "Presione una tecla para continuar"
			read -s -p "Presione \"enter\" para continuar"
			clear
			cartel "AYS - TPO - Bonfanti, Bouzo, Centena, Saracini"
		;;
		version)
			echo "version 0.99 - licencia gpl - autor bonfanti, bouzo, centena, saracini"
			exit 0
			;;
		*)
			echo "error desconocido"
			exit 5
			;;
	esac
}

#Funcion chequeo de paramentro verifica que ingreso sea correcto

chequeoDeParametros(){

if [[ -z $1 ]]  ; then 	#verifica que ingrese argumentos
	cartel "ERROR!!! Faltan parametros"
	exit 0
fi

[[ -n "$debug" ]] && cartel "CHEQUEA PARAMETROS"
while [[ -n "$1" ]]; do	#recorre los argumentos y los asigna a variables
	case "$1" in
		-v)	#muestra version
			ayuda version
			exit 0
		;;
	        -b)	#asigna backup
			backup="-b"
		;;
		-e)	#asigna variable equipos
			eq="$2"
			if [[ -z "$eq" ]] ; then
				cartel "ERROR!!! Falta Equipo"
				exit 1
			fi
			if [[ "$eq" == *","* ]] ; then	#verifica parametros separados por ","
				ip="$(echo $eq | cut -d "," -f1)"
				[[ -n "$debug" ]] && echo "--- asigna variable \$ip=$ip ---"
				port="$(echo $eq | cut -d "," -f2)"
				[[ -n "$debug" ]] && echo "--- asigna variable \$port=$port ---"
			else
				cartel "ERROR!!! Falta separador \",\""
				exit 1
			fi
			if [[ -z "$ip" ]] || [[ -z "$port" ]] ; then	#verifica ip y puerto no este vacio
				cartel "ERROR!!! Equipo o Puerto Vacios"
				exit 1
			fi
			shift
		;;
		-i)	#asigna variable persona
			pers="$2"
			if [[ -z "$pers" ]] ; then
				cartel "ERROR!!! Falta Variable Persona"
				exit 1
			elif [[ "$pers" == *","* ]] ; then	#verifica parametros separados por ","
				user="$(echo $pers | cut -d "," -f1)"
				[[ -n "$debug" ]] && echo "--- asigna variable \$user=$user ---"
				pass="$(echo $pers | cut -d "," -f2)"
				[[ -n "$debug" ]] && echo "--- asigna variable \$pass=$pass ---"
			else	#parametro pers incorrecto
				cartel "ERROR!!! Falta separasor \",\""
				exit 1
			fi
			if [[ -z "$user" ]] || [[ -z "$pass" ]] ; then
				cartel "ERROR!!! Usuario o Contraseña Vacios"
				exit 1
			fi
			shift
		;;
		-k)	#asigna archivo llaves
			key="$2"
			if ! [ -f "$key" ] ; then
				cartel "ERROR!!! Archivo Key no se encuentra"
				exit 1
			fi
			shift
		;;
		-g)	#asigna archivo personas
			grupo="$2"
			if ! [ -f "$grupo" ] ; then
				cartel "ERROR!!! Archivo Grupo no se encuentra"
				exit 1
			else
				[[ -n "$debug" ]] && echo "--- asigna variable \$grupo=$grupo  ---"
			fi
			shift
		;;
		-s)	#muestra usuarios del sistema
			if [[ -z "$login" ]] ; then
				sistema="-s"
			fi
		;;
		-l)	#muestra usuarios con login
			sistema=""
			login="-l"
		;;
		-m)	#muestra usuarios por pantalla
			if [[ -z "$quiet" ]] ; then
				pantalla="-m"
			fi
		;;
		-p)	#asigna variable persona
			pers="$2"
			if [[ -z "$pers" ]] ; then
				cartel "ERROR!!! Variable Persona Vacia"
				exit 1
			elif [[ "$pers" == *","* ]] ; then	#verifica parametros separados por ","
				user="$(echo $pers | cut -d "," -f1)"
				[[ -n "$debug" ]] && echo "--- asigna variable \$user=$user ---"
				pass="$(echo $pers | cut -d "," -f2)"
				[[ -n "$debug" ]] && echo "--- asigna variable \$pass=$pass ---"
			else	#parametro pers incorrecto
				cartel "ERROR!!! Falta separador \",\""
				exit 1
			fi
			if [[ -z "$user" ]] || ! [[ "$user" =~ ^[[:alnum:]]+$ ]] ; then	#verifica que tenga nombre de usuario y sea alfanuerico
				cartel "ERROR!!! Variable Persona Vacia o Caracteres Incorrectos"
				exit 1
			fi
			shift
		;;
		-o)	#asigna variable persona
			pers="$2"
			if [[ -z "$pers" ]] ; then
				cartel "ERROR!!! Variable Persona Vacia"
				exit 1
			elif [[ "$pers" == *","* ]] ; then	#verifica parametros separados por ","
				user="$(echo $pers | cut -d "," -f1)"
				[[ -n "$debug" ]] && echo "--- asigna variable \$user=$user ---"
			else
				user="$pers"
			fi
			if [[ -z "$user" ]] ; then
				cartel "ERROR!!! Variable Usuario Vacia"
				exit 1
			fi
			shift
		;;
		-r)	#asigna archivo equipos
			red="$2"
			if ! [ -f "$red" ] ; then
				cartel "ERROR!!! Archivo Red Vacio"
				exit 1
			fi
			shift
		;;
		*)	#argumento desconocido
			cartel "ERROR DESCONOCIDO!!!"
			exit 1
		;;
	esac
	shift
done
}

export -f verificarRoot
export -f borde
export -f centro
export -f cartel
export -f randPass
export -f buscaProg
export -f verificaComando
export -f ayuda
export -f chequeoDeParametros
