#!/bin/bash
# este script permite los usuarios activos en sistema
# tambien tiene 3 metodos de salida: 
# salida normal, solo informa los usuarios creados con sus contraseñas
# finalizando, genera un archivo csv que va anexando los distintos usuarios

#login.defs archivo de configurancion, control de login 
#Se lee el archivo login.defs, se busca la linea que comienza con UID_MIN y UID_MAX , se eliminan los espacios / tabulaciones consecutivos. se define el un separador de campos y se toma el segundo campo.
#los valores UID_MIN y UID_MAX es el rango de  id de usuarios definidad para usuarios Regulares (normales) que pueden iniciar sesion

chequeoDeParametros $@

if [[ -z "$sistema" ]] && [[ -z "$login" ]] ; then	#si no se seleciono logins o sistemas, asigna login
	login="-l"
fi
if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
	if ! ( mkdir log &> /dev/null ) ; then 
		cartel "ERROR!!! No puede crear carpeta"
	fi
fi
if [[ -n $sistema ]] ; then	#si selecciono sistema, crea archivo con cabecera
	echo "user,home,shell" > ./log/lista_sistemas.csv
fi
if [[ -n $login ]] ; then	#si selecciono usuarios con shell, crea archivo con cabecera
	echo "user,home,shell" > ./log/lista_usuarios.csv
fi

uid_min=$(cat /etc/login.defs | grep ^UID_MIN | sed 's/\s\+/,/g' | cut -d "," -f 2)	#toma uid min de usuario
uid_max=$(cat /etc/login.defs | grep ^UID_MAX | sed 's/\s\+/,/g' | cut -d "," -f 2)	#toma uid max de usuario

OIFS=$IFS # Guarda el valor de IFS 
IFS=: # /etc/passwd usa ":"para separar los campos

while read user pass uid gid detalle home shell ; do	#recorre uno a uno los usuarios y toma variables
	if ( [[ $uid -lt $uid_min ]] || [[ $uid -gt $uid_max ]] ) && [[ -n $sistema ]]; then	#verifica rangos de uid
		echo "$user,$home,$shell" >> ./log/lista_sistemas.csv	#agrega linea al archivo
		if [[ -n $pantalla ]] ; then	#si se llamo con opcion -m, muestra por pantalla
			echo "$user,$home,$shell"
		fi
	fi
	if ( [[ $uid -ge $uid_min ]] && [[ $uid -le $uid_max ]] ) && ( grep "$shell" /etc/shells &> /dev/null && [[ -n "$login" ]] ) ; then	#verifica rangos de uid que tenga shell valida
		echo "$user,$home,$shell" >> ./log/lista_usuarios.csv	#agrega linea al archivo
		if [[ -n $pantalla ]] ; then	#si se llamo con opcion -m, muestra por pantalla
			echo "$user,$home,$shell"
		fi
	fi
done < /etc/passwd	#asigna archivo a recorrer

IFS=$OIFS # Recupera el $IFS original

if [[ -n $sistema ]] ; then	#si se genero usuarios sistema muestra este cartel
	cartel "Se ha Generado el archivo con usuarios \"lista_sistemas.csv\""
fi

if [[ -n $login ]] ; then	#si se genero usuarios login muestra este cartel
	cartel "Se ha Generado el archivo con usuarios \"lista_usuarios.csv\""
fi
chmod -Rf 777 log &> /dev/null

