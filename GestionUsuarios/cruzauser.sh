#!/bin/bash
# este script permite crear y cruzar llaves de usuarios a equipos
# los datos pueden ser ingresados por parametro o por un archivo csv
# este ultimo debe contener el usuario y contraseña del usuario
# en el caso de los equipos debe tener ip o nombre de dominio y puerto
# si no se provee equipos, este generara solamente las llaves
# tambien tiene 3 metodos de salida:
# salida normal, solo informa los usuarios creados con sus contraseñas
# finalizando, genera un archivo con el nombre de usuario
# contiene su nombre de usuario y su llave
# ademas genera otro archivo csv que va anexando los distintos usuarios
# y sus accesos

verificarRoot

chequeoDeParametros $@

if [[ -n "$grupo" ]] ; then
	while read pers; do	#recorre archivo pesronas
		if [[ -n "$red" ]] ; then
			while read eq; do	#recorre archivo equipos
				if [[ -n "$key" ]] ; then
					$0 -k $key -i $pers -e $eq	#llama al programa con una persona individual y un equipo individual con llave especifica
				else
					$0 -i $pers -e $eq	#llama al programa con una persona individual y un equipo individual sin llave
				fi
			done < $red
		elif [[ -n "$eq" ]] ; then
			if [[ -n "$key" ]] ; then
				$0  -k $key -i $pers -e $eq	#llama al programa con una persona individual y un equipo individual con llave especifica
			else
				$0 -i $pers -e $eq	#llama al programa con una persona individual y un equipo individual sin llave
			fi
		elif [[ -z "$key" ]] ; then
			$0 -i $pers	#llama al programa con una persona individual
		else	
			cartel "ERROR !!! No paso equipos"
			exit 1
		fi
	done < $grupo
elif [[ -n "$pers" ]] ; then
	if [[ -n "$red" ]] ; then
		while read eq; do	#recorre archivo equipos
			if [[ -n "$key" ]] ; then
				$0 -k $key -i $pers -e $eq	#llama al programa con una persona individual y un equipo individual con llave especifica
			else
				$0 -i $pers -e $eq	#llama al programa con una persona individual y un equipo individual sin llave
			fi
		done < $red
	elif [[ -n "$eq" ]] ; then
		if [[ -z $key ]] ; then	#verifica si esta asignada la variable llave
			key="/home/$user/.ssh/id_rsa.pub"
			if ! [[ -f "$key" ]] ; then	#verifica que exista el archivo llave
				$0 -i $pers	#llama al programa con una persona individual
			fi
			$0 -k $key -i $pers -e $eq	#llama al programa con una persona individual y un equipo individual con llave especifica
		else 
			if ! $(ping -c 1 -q $ip &>/dev/null); then	#verifica conexion con equipo
				cartel "ERROR!!! No Conecta"
				exit 1
			fi
			if $(which sshpass &>/dev/null) ; then	#verifica programa sshpass
				if $(cat $key | sshpass -p $pass ssh -o StrictHostKeyChecking=no -p $port $user@$ip "mkdir -p ~/.ssh && cat >> /home/$user/.ssh/authorized_keys" &>/dev/null); then	#copiando llave
					echo "LLave Cruzada. Acceso: ssh -p $port $user@$ip"	#muestra por salida normal
					if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
						mkdir log &> /dev/null
					fi
					echo "ssh -p $port $user@$ip" >> ./log/llave_usuario.csv	#agrega registro general
					cartel "SSH - $(date +%X\ -\ %d/%m/%Y)" >> ./log/$user.txt	#agrega registro usuario
					centro "ssh -p $port $user@$ip" >> ./log/$user.txt
					borde >> ./log/$user.txt
					chmod -Rf 777 log &> /dev/null
				else
					cartel "ERROR!!! No pudo cruzar llave"
					exit 1
				fi
			else	#no encuenta el programa sshpass, copia manualmente
				echo "Ingresar pass manualmente: $pass"
				if $(cat $key | ssh -o StrictHostKeyChecking=no -p $port $user@$ip "mkdir -p ~/.ssh && cat >> /home/$user/.ssh/authorized_keys" &>/dev/null); then	#copiando llave
					echo "LLave Cruzada. Acceso: ssh -p $port $user@$ip"	#muestra por salida normal
					if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
						mkdir log &> /dev/null
					fi
					echo "ssh -p $port $user@$ip" >> ./log/llave_usuario.csv	#agrega registro general
					cartel "SSH - $(date +%X\ -\ %d/%m/%Y)" >> ./log/$user.txt	#agrega registro usuario
					centro "ssh -p $port $user@$ip" >> ./log/$user.txt
					borde >> ./log/$user.txt
					chmod -Rf 777 log &> /dev/null
				else
					cartel "ERROR!!! No pudo cruzar llave"
					exit 1
				fi
			fi
		fi
	elif [[ -z $key ]] ; then	#no paso equipos y tampoco llave, entonces se desea generar la llave
		key="/home/$user/.ssh/id_rsa"	#asigna archivo para verificar si existe
		if [[ -f "$key" ]] ; then	#verifica si existe archivo
			echo "$key.pub"
		else
			if ! $(/bin/grep -q "^$user:" /etc/passwd &>/dev/null) ; then	#verifica si existe usuario 
				cartel "ERROR!!! No existe usuario"
				exit 1
			fi
			if ! $(echo -ne "$pass\n" | su $user -c "exit" &>/dev/null) ; then	#verifica acceso con usuario y contraseña
				cartel "ERROR!!! No pudo loguear usuario"
				exit 1
			fi
			if ! $(echo -ne "$pass\n" | su $user -c "ssh-keygen -q -t rsa -f $key &>/dev/null" &>/dev/null) ; then	#intenta crear llave
				cartel "ERROR!!! No pudo crear llave"
				exit 1
			fi
			echo "Llave creada: $user,$key.pub"	#muestra por salida normal
			if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
				mkdir log &> /dev/null
			fi
			echo "$user,$key" >> ./log/llave_usuario.csv	#agrega registro general
			cartel "LLAVE - $(date +%X\ -\ %d/%m/%Y)" >> ./log/$user.txt	#agrega registro usuario
			centro $(cat $key.pub) >> ./log/$user.txt
			borde >> ./log/$user.txt
			chmod -Rf 777 log &> /dev/null
		fi
	else
		cartel "ERROR!!! Paso solamente usuario"
		exit 3
	fi
else
	cartel "ERROR!!! No paso usuario"
	exit 1
fi
