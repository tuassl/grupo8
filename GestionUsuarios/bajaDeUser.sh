#!/bin/bash
# este script permite dar de baja usuarios del sistema
# los datos pueden ser ingresados por parametros o por un archivo csv
# este ultimo debe contener el usuario
# tambien tiene 3 metodos de salida
# salida normal, informa usuarios y archivo de backup en caso de ser generado
# finalizando, genera un archivo con el nombre de usuario
# contiene su nombre de usuario y la ubicacion de su archivo de backup
# ademas genera otro archivo csv que va anexando los distintos usuarios
# que se van eliminando

verificarRoot

chequeoDeParametros $@

if [[ -n "$grupo" ]] ; then
	while read pers; do	#recorre archivo personas
		$0 $backup -p $pers	#llama al programa con una persona individual
	done < $grupo
elif [[ -n "$pers" ]] ; then
	if ! $(/bin/grep -q "^$user:" /etc/passwd &>/dev/null); then	#verifica si existe usuario
		cartel "ERROR!!! No encuentra usuario"
		exit 1
	fi
	if $(killall -KILL -u $user &>/dev/null) ; then	#intenta matar todos los procesos del usuario
		cartel "ERROR!!! No mata procesos"
		exit 1
	fi
	if [[ "$backup" = "-b" ]] ; then	#verifica variable backup
		if ! $(/sbin/deluser --remove-home --backup-to /home "$user" &>/dev/null) ; then	#intenta borrar usuario con backup
			cartel "ERROR!!! No puede eliminar usuario"
			exit 1
		else
			echo "Usuario eliminado: $user,/home/$user.tar.bz2"	#muestra por salida normal
			if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
				mkdir log &> /dev/null
			fi
			echo "$user,/home/$user.tar.bz2" >> ./log/baja_usuario.csv	#agrega registro general
			cartel "BAJA - $(date +%X\ -\ %d/%m/%Y)" >> ./log/$user.txt	#agrega registro usuario
			centro "Usuario: $user" >> ./log/$user.txt
			centro "Backup: /home/$user.tar.bz2" >> ./log/$user.txt
			borde >> ./log/$user.txt
			chmod -Rf 777 log &> /dev/null
		fi
	else	#intenta borrar usuario sin backup
		if ! $(/sbin/deluser --remove-home "$user" &>/dev/null) ; then
			cartel "ERROR!!! No puede eliminar usuario"
			exit 1
		else
			echo "Usuario eliminado: $user"	#muestra por salida normal
			if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
				mkdir log &> /dev/null
			fi
			echo "$user" >> ./log/baja_usuario.csv	#agrega registro general
			cartel "BAJA - $(date +%X\ -\ %d/%m/%Y)" >> ./log/$user.txt	#agrega registro usuario
			centro "User: $user" >> ./log/$user.txt
			borde >> ./log/$user.txt
			chmod -Rf 777 log &> /dev/null
		fi
	fi
else
	cartel "ERROR!!! No paso usuario"
	exit 1
fi
