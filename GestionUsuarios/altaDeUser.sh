#!/bin/bash
# este script permite generar el alta de usuarios en sistema
# los datos pueden ser ingresados por parametro o por un archivo csv
# este ultimo debe contener el usuario y si se desea, la contraseña
# si no se provee la contraseña, esta se genera aleatoriamente
# tambien tiene 3 metodos de salida:
# salida normal, solo informa los usuarios creados con sus contraseñas
# finalizando, genera un archivo con el nombre de usuario
# contiene su nombre de usuario y su conntraseña
# ademas genera otro archivo csv que va anexando los distintos usuarios
# que se van creando

verificarRoot

chequeoDeParametros $@

if [[ -n "$grupo" ]] ; then
	while read pers; do	#recorre archivo personas
		$0 -p $pers	#llama al programa con una persona individual
	done < $grupo
elif [[ -n "$pers" ]] ; then
	if $(/bin/grep -q "^$user:" /etc/passwd &>/dev/null); then	#verifica si existe usuario
		cartel "ERROR!!! Usuario ya existe"
		exit 1
	fi
	if [ -z "${pass}" ] ; then	#verifica si tiene contraseña vacia
		pass=$(randPass)
	fi
	encpass=$(perl -e 'print crypt($ARGV[0], "password")' $pass)	#encripta la contraseña para pasarla por parametro
	if ! $(/sbin/useradd -m -s /bin/bash -p $encpass $user &>/dev/null) ; then	# intenta creando usuario
		cartel "ERROR!!! No se puede crear usuario"
	else
		echo "Usuario creado: $user,$pass"	#muestra por salida normal
		if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea
			mkdir log &> /dev/null
		fi
		echo "$user,$pass" >> ./log/alta_usuarios.csv	#agrega registro general
		cartel "ALTA - $(date +%X\ -\ %d/%m/%Y)" >> ./log/$user.txt	#agrega registro usuario
		centro "Usuario: $user" >> ./log/$user.txt
		centro "Contraseña: $pass" >> ./log/$user.txt
		borde >> ./log/$user.txt
		chmod -Rf 777 log &> /dev/null
	fi
else
	cartel "ERROR!!! No paso usuario"
	exit 1
fi
