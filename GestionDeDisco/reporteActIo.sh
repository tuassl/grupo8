#Reporte de actividad de I/O en todos los discos primarios de todos los servidores provistos . 
#Deberá obtener las lecto/escrituras del comando que se está ejecutando en ese momento. 
#Al igual que en el caso anterior, en caso que la herramienta no se encuentre instalada, el script deberá notificar al usuario y ofrecerle la posibilidad de instalarla 
#Ayuda: iotop, considere las opciones -o -n y -b).

verificarRoot

declare -i numero

cartel "Informacion de actividad de I/O de discos primarios."
centro "Se van a realizar iteraciones de 1 segundo de diferencia para realizar el monitoreo"
borde
if verificaComando iotop ; then #comprueba que iotop este instalado 
	echo ""
	read -p "Indique el numero de Iteraciones de Analisis que desea hacer: " numero #pregunta la cantidad de veces que quiere realizar un analicis de disco
	while [[ $numero -eq 0 ]]; #debe ingresar un numero (valor) valido
	do
		cartel "Valor Invalido."
		echo ""
		read -p "Indique el numero de Iteraciones de Analisis que desea hacer: " numero
	done
	cartel "Se procede a realizar el analisis.."
	sleep 2
	iotop -o -b -n $numero #realiza analisis
fi 
