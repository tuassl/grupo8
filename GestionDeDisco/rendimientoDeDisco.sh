#!/bin/bash

#Reporte de rendimiento de los discos locales, en caso que la herramienta
#no se encuentre instalada, el script deberá notificar al usuario y ofrecerle
#la posibilidad de instalarla (Ayuda: sysstat).

verificarRoot

if verificaComando sysstat; then

reporte=log/disco.csv
log=$(dirname $reporte)

# Crea el directorio si no existe
test ! -d "$log" && mkdir -p "$log"
	if [ ! -s "$reporte" ]; then

#captura las estadísticas del dispositivo.
#Exemplo `iostat -d`
#Device:            tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
#sda              17,58        70,29       240,89   43227940  148144276

		echo '"Device";"tps";"kB_read/s";"kB_wrtn/s";"kB_dscd/s";"kB_read";"kB_wrtn";"kB_dscd"' > "$reporte"
		iostat -d | tail -n5 | head -n5 | awk -v 's="' '{ print s$1s ";" s$2s ";" s$3s ";" s$4s ";" s$5s ";" s$6s ";" s$7s ";" s$8s }' >> "$reporte"

	fi
	chmod 777 -R $log
	cartel "Reporte de rendimiento de los discos locales"
	iostat -s -d -p | head -n6
	cartel "Se ha generado el archivo con reporte de disco \"log/disco.csv\""
fi
