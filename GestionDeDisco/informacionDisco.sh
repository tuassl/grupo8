#!/bin/bash

#Este script lo que haces es conectase a maquina remota por ssh y consulta la informacion del disco principal de la maquina
declare -i porcentajeuso;

cartel "Informacion sobre el espacio de disco disponible."
centro "Que desea monitorear?:"
borde
echo -ne "
	1) Maquina local
	2) Maquina remota en especifico
	3) Listado de Maquinas Remotas
	0) Atras
"
read -p "Opcion: " opcion

case $opcion in
	1)
		cartel "Informacion de Discos de la Maquina Local"
		df -Th | grep ^/dev/ | 
			while read ST tipo tam usa disp puso montado;
			do
				echo "Sistema de Fichero: $ST"
				echo "Tipo: $tipo"
				echo "Tamaño: " $tam
				echo "% de Uso: $puso"
				porcentajeuso=$(echo "$puso" | tr -d %)
				porcentajeuso=100-$porcentajeuso
				echo "% Disponible: $porcentajeuso%"
				echo "Punto de Montaje: $montado"
				echo "---------------------------------------"
			done
	;;
	2)
		cartel "Especifique la IP de la maquina"
		read -p "IP:" ip
		cartel "Especifique el puerto donde se conecta"
		read -p "PUERTO(por defecto 22): " puerto
		if [ -z $puerto ]; then
			puerto=22
		fi
		cartel "Informacion de Maquina con IP: $ip y Puerto: $puerto"
		if $(which sshpass &>/dev/null) ; then
			sshpass -p "administrador" ssh -p $puerto administrador@$ip df -Th | grep ^/dev/ | #se conecta a la maquina remota mediante el "usuario administrador y ejecuta el comando df -Th" 
				while read ST tipo tam usa disp puso montado; # se recorre la informacion de del comando
				do
					echo "Sistema de Fichero: $ST"
					echo "Tipo: $tipo"
					echo "Tamaño: " $tam
					echo "% de Uso: $puso"
					porcentajeuso=$(echo "$puso" | tr -d %)
					porcentajeuso=100-$porcentajeuso
					echo "% Disponible: $porcentajeuso%"
					echo "Punto de Montaje: $montado"
					echo "---------------------------------------"
				done
		else
			echo "Ingresar pass manualmente: administrador"
			ssh -p $puerto administrador@$ip df -Th | grep ^/dev/ | #se conecta a la maquina remota mediante el "usuario administrador y ejecuta el comando df -Th" 
				while read ST tipo tam usa disp puso montado; # se recorre la informacion de del comando
				do
					echo "Sistema de Fichero: $ST"
					echo "Tipo: $tipo"
					echo "Tamaño: " $tam
					echo "% de Uso: $puso"
					porcentajeuso=$(echo "$puso" | tr -d %)
					porcentajeuso=100-$porcentajeuso
					echo "% Disponible: $porcentajeuso%"
					echo "Punto de Montaje: $montado"
					echo "---------------------------------------"
				done
		fi
	;;
	3)
		cartel "Especifique la path del archivo de maquinas a monitorear"
		read -p "Path: " archivo
		if [ -f $archivo ]; then
			OIFS=$IFS
			IFS=","
			while read ip puerto; #usamos el archivo red.csv para ver las maquinas a las cual conectarse
			do
				IFS=$OIFS
				cartel "Informacion de Maquina con IP: $ip y Puerto: $puerto"
				if $(which sshpass &>/dev/null) ; then
					sshpass -p "administrador" ssh -n -p $puerto administrador@$ip "df -Th | grep ^/dev/" | #conecta a la maquina remota con el usuario administrador y ejecuta el comando df -Th
					while read ST tipo tam usa disp puso montado; # se recorre la informacion de del comando
					do
						echo "Sistema de Fichero: $ST"
						echo "Tipo: $tipo"
						echo "Tamaño: " $tam
						echo "% de Uso: $puso"
						porcentajeuso=$(echo "$puso" | tr -d %)
						porcentajeuso=100-$porcentajeuso
						echo "% Disponible: $porcentajeuso%"
						echo "Punto de Montaje: $montado"
						echo "---------------------------------------"
					done
				else
					echo "Ingresar pass manualmente: administrador"
					ssh -n -p $puerto administrador@$ip "df -Th | grep ^/dev/" | #conecta a la maquina remota con el usuario administrador y ejecuta el comando df -Th
					while read ST tipo tam usa disp puso montado; # se recorre la informacion de del comando
					do
						echo "Sistema de Fichero: $ST"
						echo "Tipo: $tipo"
						echo "Tamaño: " $tam
						echo "% de Uso: $puso"
						porcentajeuso=$(echo "$puso" | tr -d %)
						porcentajeuso=100-$porcentajeuso
						echo "% Disponible: $porcentajeuso%"
						echo "Punto de Montaje: $montado"
						echo "---------------------------------------"
					done
				fi
				IFS=","
			done < $archivo
			IFS=$OIFS
		else
			cartel "No Existe el archivo. Adios!"
		fi
	;;
esac
