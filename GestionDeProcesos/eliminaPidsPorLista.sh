#!/bin/bash

#Extienda la funcionalidad del punto anterior, para eliminar una lista
#de procesos (PIDs) de manera de automatizar el trabajo de hacerlo uno por uno.

#Importante: Para poder probar este ejercicio, deberá crear un script aparte
#que ejecute 10 instancias del proceso que Ud. desee y luego deberá obtener
#los id de proceso de ese programa y guardarlos en un archivo para alimentar
#la funcionalidad descripta.

verificarRoot

source GestionDeProcesos/ejecucionDeInstancia.sh;

creaInstancia

cartel "Se eliminaran procesos (PIDs) por medio de una lista"

kill15=0
kill9=0

while read PID; do

  if (ps aux $PID &> /dev/null); then

     if !(kill -15 $PID &> /dev/null); then

       kill15=$(($kill15+1))

    else

        if !(kill -9 $PID &> /dev/null); then

          kill9=$(($kill9+1))

        else

           cartel "No se pudo elimnar el proceso de manera amable y ni forzado"

        fi

   fi 

 else

     cartel "El id de proceso (PID) no existe"

 fi

done < pid.csv

cartel "Se eliminaron $kill15 procesos amable y $kill9 procesos forzado "

rm pid.csv
