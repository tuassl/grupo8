#!/bin/bash

#Reportar una alerta si el número de procesos es superior a
#un umbral(entrada) provisto por el usuario

verificarRoot

cartel "Reporte de procesos"
centro "Se verificara si la cantidad de procesos supera el umbral"
borde
read -p " Ingrese un valor para verificar la cantidad de procesos: " umbral

if [ $(ps aux --no-heading | wc -l) -gt $umbral ]; then

    cartel "La cantidad de procesos es superior ($(ps aux --no-heading | wc -l))"

    cartel "Proceso de CPU superior usando el comando top"

# Verificar Tareas total, ejecutar, hibernar, detener, zombie

    echo "$(top -bn1 | head -10)"

    cartel "Proceso principal de la CPU usando el comando ps"

# verifica pcpu,pid,user,args

    echo "$(ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10)"

  else

    cartel "Se encuentra con la cantidad de procesos adecuado"

fi
