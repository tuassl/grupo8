#!/bin/bash

#Monitorear un proceso específico, dado un id de proceso (PID) deber mostrar
#por pantalla sólo ese proceso con un límite de iteración de 10 vece

cartel "Se van a realizar 10 iteraciones de 1 segundo de diferencia para realizar el monitoreo"
centro "Ingrese PID del proceso que desea chequear"
borde

read -p "PID: " pid

for i in `seq 1 10`; do
	cartel "Iteracion $i"
	ps -u -p $pid
	sleep 1
done
