#!/bin/bash

#Extienda la funcionalidad del punto anterior, para eliminar una lista
#de procesos (PIDs) de manera de automatizar el trabajo de hacerlo uno por uno.

#Importante: Para poder probar este ejercicio, deberá crear un script aparte
#que ejecute 10 instancias del proceso que Ud. desee y luego deberá obtener
#los id de proceso de ese programa y guardarlos en un archivo para alimentar
#la funcionalidad descripta.

creaInstancia(){

for i in $(seq 1 10); do

   $(yes &> /dev/null &)

done

ps ax | grep yes | awk -v 's=' '{ print s$1s }' > pid.csv

cat pid.csv

}

export -f creaInstancia
