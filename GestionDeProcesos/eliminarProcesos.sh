#!/bin/bash

#Eliminar un proceso específico dado su id de proceso (PID). Debe realizar un intento de
#eliminarlo de manera amable; en caso de no funcionar intentar con mayor ímpetu y por ultimo,
#en caso de no poder eliminarlo, emitir una advertencia.

verificarRoot

cartel "Vamos a eliminar un proceso especifico dado su id de proceso (PID)"

declare -u i
declare -i OPCION

i=Y

while [ "Y" == $i ]; do

 read -p "Ingrese id de proceso (PID) a eliminar: " OPCION

  if (ps -aux $OPCION &> /dev/null); then

     cartel "Vamos a intentar eliminar el proceso"

     if !(kill -15 $OPCION &> /dev/null); then

        cartel "Se elimino de manera amable"

    else

        if !(kill -9 $pid &> /dev/null); then

           cartel "Se elimino de forma forzada"

        else

           cartel "No se puede elimnar el proceso"

        fi

   fi

 else

     cartel "El id de proceso (PID) no existe"

 fi

 read -p "Desea eliminar otro id de proceso (PID) (Y/N): " i

done




