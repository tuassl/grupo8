#!/bin/bash

#Reporte de los primeros 5 procesos ordenados por consumo de memoria.

cartel "Reporte de Primeros 5 Procesos"

cartel "Desea monitorear los procesos de:"
echo -ne "
	1) Maquina local
	2) Maquina en especifico
	3) Listado de Maquinas
	0) Atras
"
read -p "Opcion: " opcion

case $opcion in
	1)
		cartel "Informacion de Procesos Maquina Local"
		ps -au --sort -pmem | head -n6
		;;
	2)
		cartel "Especifique la IP de la maquina"
		read -p "IP:" ip
		cartel "Especifique el puerto donde se conecta"
		read -p "PUERTO(por defecto 22): " puerto
		if [ -z $puerto ]; then
			puerto=22
		fi
		cartel "Informacion de Procesos en IP: $ip y Puerto: $puerto"
		if $(which sshpass &>/dev/null) ; then
			sshpass -p "administrador" ssh -p $puerto administrador@$ip ps -au --sort -pmem | head -n6
		else
			echo "Ingresar pass manualmente: administrador"
			ssh -p $puerto administrador@$ip ps -au --sort -pmem | head -n6
		fi
		;;
	3)
		cartel "Especifique la path del archivo de maquinas"
		read -p "Path: " archivo
		
		if [ -f $archivo ]; 
		then
			OIFS=$IFS
			IFS=,
			while read ip puerto;
			do
				cartel "Informacion de Procesos en IP: $ip y Puerto: $puerto"
				if $(which sshpass &>/dev/null) ; then
					sshpass -p "administrador" ssh -n -p $puerto administrador@$ip ps -au --sort -pmem | head -n6
				else
					echo "Ingresar pass manualmente: administrador"
					ssh -n -p $puerto administrador@$ip ps -au --sort -pmem | head -n6
				fi
			done < $archivo
		else
			cartel "No Existe el archivo. Adios!"
		fi
		;;
	0) exit 1;;


	*) cartel "Opcion Equivocada";;

esac
