#!/bin/bash
#set -n 

#agrega funciones y ayudas, por si no se esta ejecuntado el sistema desde el menu
if ! [[ "$(type -t ayuda 2> /dev/null)" = "function" ]] ; then
	source ../FuncionAGM/funcionAyuda.sh
fi
if ! [[ "$(type -t cartel 2> /dev/null)" = "function" ]] ; then
	source ../FuncionAGM/funcionesGenerales.sh
fi
cartel "Reporte de uso de Memoria"

cartel "Desea monitorear la memoria de:"
echo -ne "
	1) Maquina local
	2) Maquina en especifico
	3) Listado de Maquinas
	0) Atras	
"
read -p "Opcion: " opcion

case $opcion in
	1)	
		cartel "Analisis con free , vmstat y meminfo"
		free -h
		borde
		vmstat
		borde
		cat /proc/meminfo | head -n16
		;;
	2)
		cartel "Especifique la IP de la maquina"
		read -p "IP:" ip
		cartel "Especifique el puerto donde se conecta"
		read -p "PUERTO(por defecto 22): " puerto
		if [ -z $puerto ]; then
			puerto=22	
		fi

		#Coincidere esto como una sola linea.Donde en la MV donde se conecta de forma remota se crean las funciones necesarias para formar el cartel (visual)
		#Con dicho cartel se separan las secciones de monitoreo, con free, vmstat y el archivo meminfo
#		ssh -p $puerto administrador@$ip 'borde(){ for guion in {1..80} ; do echo -n "-" ; done ; echo ; } ; centro(){ texto="$@" ; while (( ${#texto} >= 75 )) ; do echo "| ${texto:0:75}  |" ; texto=${texto:75:((${#texto}-75))} ; done ; echo -ne "| $texto" ; for espacio in $(seq 1 $((75-${#texto}))) ; do echo -ne " " ; done ; echo "  |" ; } ; cartel(){ borde ; centro $@ ; borde ; } ; cartel "Con free" ; free -h ; cartel "Con vmstat" ; vmstat ; cartel "Archivo meminfo" ; cat /proc/meminfo '

		cartel "Analisis con free , vmstat y meminfo"
		ssh -n -p $puerto administrador@$ip free -h
		borde
		ssh -n -p $puerto administrador@$ip vmstat
		borde
		ssh -n -p $puerto administrador@$ip cat /proc/meminfo | head -n16
		;;
	3)
		cartel "Especifique la path del archivo de maquinas a monitorear"
		read -p "Path: " archivo
		if [ -f $archivo ]; then
			OIFS=$IFS
			IFS=,
			while read ip puerto; 
			do
				cartel "Maquina con IP: $ip y Puerto: $puerto"

				#Mismo que Opcion 2

#				ssh -n -p $puerto administrador@$ip 'borde(){ for guion in {1..80} ; do echo -n "-" ; done ; echo ; } ; centro(){ texto="$@" ; while (( ${#texto} >= 75 )) ; do echo "| ${texto:0:75}  |" ; texto=${texto:75:((${#texto}-75))} ; done ; echo -ne "| $texto" ; for espacio in $(seq 1 $((75-${#texto}))) ; do echo -ne " " ; done ; echo "  |" ; } ; cartel(){ borde ; centro $@ ; borde ; } ; cartel "Con free" ; free -h ; cartel "Con vmstat" ; vmstat ; cartel "Archivo meminfo" ; cat /proc/meminfo '

			cartel "Analisis con free , vmstat y meminfo"
			ssh -n -p $puerto administrador@$ip free -h
			borde
			ssh -n -p $puerto administrador@$ip vmstat
			borde
			ssh -n -p $puerto administrador@$ip cat /proc/meminfo | head -n16

			done < $archivo
		else
			cartel "No Existe el archivo. Adios!"
		fi
		;;
	0) exit 1;;


	*) cartel "Opcion Equivocada";;

esac
IFS=$OIFS
