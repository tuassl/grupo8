#!/bin/bash

#Comprueba el porcentaje de uso de memoria ram y si supera el 20 % envia un mensaje al mailbox local del usuario root de la maquina que ejecuta la aplicacion.
#Ademas crea un archivo temporal en /tmp/
verificaComando bc
verificaComando mailutils

ramusage=$(free | awk '/Mem/{printf("Uso de memoria RAM: %.2f\n"), $3/$2*100}'| awk '{print $5}')

read -p "Ingrese un valor umbral para la comprobracion de % de uso de Memoria Ram (por defecto 20): " umbral
if [[ -z $umbral ]];
then
	umbral=20;
fi
if (( $(echo "$ramusage > $umbral" |bc -l) )); then
cartel "Advertencia uso actual: $ramusage %"
SUBJECT="ATENCIÓN: El uso de memoria es alto en $(hostname), fecha: $(date)"
MESSAGE="/tmp/Mail.out"
TO=$USER
fecha=$(date +"%d-%m-%y.%H:%M:%S")

if ! [[ -d log ]] ; then	#verifica carpeta log, sino la crea con permisos
	mkdir -m 777 log &> /dev/null
fi
TEMPORAL=$(mktemp -p log usoram.$(hostname).$fecha.XXXX);

	echo "La cantidad de memoria en uso es: $ramusage%" >> $MESSAGE
	echo "" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "Consumo de memoria con información de TOP" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "$(top -b -o +%MEM | head -n 20)" >> $MESSAGE
	echo "" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "Consumo de memoria con información de PS" >> $MESSAGE
	echo "------------------------------------------------------------------" >> $MESSAGE
	echo "$(ps -eo pid,ppid,%mem,%cpu,cmd --sort=-%mem | head)" >> $MESSAGE
	mail -s "$SUBJECT" "$TO" < $MESSAGE

	echo "ASUNTO: $SUBJECT" > $TEMPORAL;
	echo "PARA $TO" >> $TEMPORAL;
	echo "" >> $TEMPORAL;
	cat $MESSAGE >> $TEMPORAL;
	rm /tmp/Mail.out
	cartel "Se ha enviado un informe al mailbox local del usuario administrador."
	cartel "Se ha generado el archivo temporal en  GestionDeMemoria/$TEMPORAL"
else
	cartel "No es necesario enviar ningun reporte, el porcentaje de uso de Memoria Ram no alcalza el Umbral establecido."

fi

